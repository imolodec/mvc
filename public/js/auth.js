/**
 * Created by serg on 21.01.16.
 */

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}

function signIn(login, password){

    var loginError = false;

    $.ajax({
        method:'POST',
        data: {
            'login': login,
            'password': password
        },
        url: "/users/auth",
        error: function(){
            loginError = true;
        },
        success: function(data) {
            //alert( "Прибыли данные: " + data );
            if (+data == 200) {
                loginError = false;
                // $('#info-block').text('You are welcome!!!');
               // $('#login-block').remove();
            }else {
                $('#info-block').text('Authentication error');
                loginError = true;
            }
        },
        complete: function(){
            if (loginError == false) {
                var user =  getCookie('user');
                var userObj = JSON.parse(user);

                afterLogin(userObj.login);
            }
        }
    });
    return false;
}

function afterLogin(userName){
    $('#SignInModal').modal('hide');
    $('#user-profile-li').removeClass('hidden');
    $('#sign-in-li').addClass('hidden');
    $('#user-name-link').text(userName);

    var redirectTo = getCookie('redirectTo');
    if (redirectTo != undefined) {
        deleteCookie('redirectTo');
        window.location.replace(redirectTo);
    }
}


function registerNewUser(login, password) {
    var loginError = false;

    $.ajax({
        method: 'POST',
        data: {
            'login': login,
            'password': password
        },
        url: "/users/registration",
        error: function () {
            loginError = true;
        },
        success: function (data) {
            //alert( "Прибыли данные: " + data );
            switch (+data) {
                case 200:
                    $('#info-block').text('Registered!!!');
                    signIn(login, password);
                    loginError = false;
                    break;
                case 400:
                    $('#info-block').text('Authentication error - user already exist');
                    loginError = true;
                    break;
                case 500:
                    $('#info-block').text('Authentication error - can\'t add record to DB');
                    loginError = true;
                    break;
                default :
                    break;
            }
        }
    });
    return false;
}

$('document').ready(function(){

    var getParam = window.location.search.replace("?","");
    var getArr=[];
        getParam.split("&").forEach(function(item){
                                        var key_value = item.split("=");
                                        getArr[key_value[0]] = decodeURIComponent(key_value[1]);
        });
    if (getArr["login"]==="true"){
        $('#sign-in-li').trigger('click');
    }


    //LOGIN USR TO SITE
    $('#login-button').click(function(){
        signIn($('input[name=login]').val(), $('input[name=password]').val());
    });


    //REGISTRATION OF NEW USER
    $('#reg-button').click(function() {
        var login = $('input[name=new-login]').val();
        var password = $('input[name=new-password]').val();

        registerNewUser(login, password);

    });


});