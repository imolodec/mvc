/**
 * Created by serg on 22.01.16.
 */


function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie+";path=/;";
}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function deleteCookie(name) {
    var date = new Date(0);
 //   date.setMinutes(date.getMinutes()+1);
    document.cookie = ""+name+"=; path=/; expires=" + date.toUTCString();

}

function getSession(param) {
    var sessionParam = undefined;

    $.ajax({
        method:'POST',
        async: false,
        data: {
            'name' : param
        },
        url: '/main/getSessionParam',
        success: function(data) {
            sessionParam = data;
        }
    });

    return sessionParam;
}

$('document').ready(function(){

    $(".nav li").removeClass("active");
    $('a[href="' + this.location.pathname + '"]').parents('li,ul').addClass('active');
    $('.modal-body .nav-tabs li:first').addClass('active');

    var userJson = getCookie('user');

    if (userJson!=undefined && userJson!=""&& userJson!="null"){
        var userObj = JSON.parse(userJson);
        $('#user-name-link').text(userObj.login);
        $('#sign-in-li').addClass('hidden');
        $('#user-profile-li').removeClass('hidden');
    } else {
        $('#sign-in-li').removeClass('hidden');
        $('#user-profile-li').addClass('hidden');
    }



    $('.my-logoff-button').click(function(){
        try {
            FB.logout(function (response) {
                //$('#fb-status').html('Please login to Facebook...');
            });
        } catch(err){

        }

        try {
            signOutGoogle();
        } catch(err) {

        }

        $('#sign-in-li').removeClass('hidden');
        $('#user-profile-li').addClass('hidden');

        $.ajax({
            method: 'POST',
            url: '/users/logoff',
            error: function(){
                console.log("Logoff Error!");
            }  ,
            success: function(){
                if(document.URL.search('/users/profile')){
                    window.location = "/main/index";
                }
            }
        });

    });



    $('#post-fb').click(function(){
        $.ajax({
            method: 'POST',
            url: '/users/postFb',
            error: function(){
                console.log("Logoff Error!");
            }  ,
            success: function(data){
                console.log(data);
            }
        });

    });

});