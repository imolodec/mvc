/**
 * Created by iMolodec on 2/1/16.
 */

/*
 Here is your client ID
 549551522124-lepnprsm1jbp7l0ksj3g16kdesvjgfod.apps.googleusercontent.com
 Here is your client secret
 T0XCkHncGhOzVs2sOzySWdvK

 */

function onSignIn(googleUser) {

    var profile = googleUser.getBasicProfile();
/*
    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    console.log("Name: " + profile.getName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());
*/

    var id_token = googleUser.getAuthResponse().id_token;
    setCookie('google_access_token',id_token);

    var name = profile.getName();
    var arrData = {id: profile.getId(),login: name, email: profile.getEmail()}
    setCookie('user', JSON.stringify(arrData));

    afterLogin(name);

};

function signOutGoogle() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}