<?php
   // session_start();
   // ini_set('display_errors', 0);
    require_once __DIR__ . '/vendor/autoload.php';
    require_once 'application/bootstrap.php';
    require_once 'application/helper/webservice_client.php';
   /*
    $web = new WebClient;
    $web->setUrl('https://gisapiapp.srv.mhp.com.ua/gis/ws/gis_ws.1cws?wsdl');
    $web->web_request();
   */


/*

class Foo
{
    public static $my_static = 'foo';

    public function staticValue() {
        return self::$my_static;
    }
}

class Bar extends Foo
{
    public function fooStatic() {
        return parent::$my_static;
    }
}


print Foo::$my_static . "\n";

$foo = new Foo();
print $foo->staticValue() . "\n";

print $foo::$my_static . "\n"; // Начиная с PHP 5.3.0
$classname = 'Foo';
print $classname::$my_static . "\n"; // Начиная с PHP 5.3.0

print Bar::$my_static . "\n";
$bar = new Bar();
print $bar->fooStatic() . "\n";

$bar::$my_static = 'bar';

print $bar::$my_static;
print $foo::$my_static;

?>
*/