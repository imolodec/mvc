<?php
/**
 * Created by PhpStorm.
 * User: iMolodec
 * Date: 12/25/15
 * Time: 10:56 PM
 */
class Route
{

    public static $counter = 0;
    static function start()
    {
        // контроллер и действие по умолчанию
        $controller_name = 'Main';
        $action_name = 'index';
        $requestUri = $_SERVER['REQUEST_URI'];

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $queryParams = explode('?', $requestUri);
            $routes = explode('/', array_shift($queryParams));
        } else {
            $routes = explode('/', $requestUri);
        }

        // получаем имя контроллера
        if ( !empty($routes[1]) )
        {
            $controller_name = $routes[1];
        }

        // получаем имя экшена
        if ( !empty($routes[2]) )
        {
            $action_name = $routes[2];
        }

        // добавляем префиксы
        $model_name = 'Model_'.$controller_name;
        $controller_name = 'Controller_'.$controller_name;
        $action_name = 'action_'.$action_name;

        // подцепляем файл с классом модели (файла модели может и не быть)

        $model_file = strtolower($model_name).'.php';
        $model_path = "application/models/".$model_file;
        if(file_exists($model_path))
        {
            include "application/models/".$model_file;
        }

        // подцепляем файл с классом контроллера
        $controller_file = strtolower($controller_name).'.php';
        $controller_path = "application/controllers/".$controller_file;
        if(file_exists($controller_path))
        {
            include "application/controllers/".$controller_file;
        }
        else
        {
            /*
            правильно было бы кинуть здесь исключение,
            но для упрощения сразу сделаем редирект на страницу 404
            */
            Route::ErrorPage404();
        }

        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }

        // создаем контроллер
        $controller = new $controller_name;
        $action = $action_name;

        if(method_exists($controller, $action))
        {
            // вызываем действие контроллера
            if (in_array($action,$controller_name::$auth_actions)) {
                include_once 'application/models/model_users.php';
                if (!Users::check_auth()){
                    setcookie("redirectTo", $requestUri, time()+60*5 , '/' );
                    header('Location: /main/index?login=true');
                    return;
                }
            }

            $controller->$action();
        }
        else
        {
            // здесь также разумнее было бы кинуть исключение
            Route::ErrorPage404();
        }

    }

    static function ErrorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }
}