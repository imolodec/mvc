<?php
/**
 * Created by PhpStorm.
 * User: iMolodec
 * Date: 12/25/15
 * Time: 11:09 PM
 */
class View
{
    //public $template_view; // здесь можно указать общий вид по умолчанию.

    function generate($content_view, $template_view, $data = null)
    {

        if(is_array($data)) {
            // преобразуем элементы массива в переменные
            extract($data);
        }

        include 'application/views/'.$template_view;
    }
}