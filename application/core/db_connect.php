<?php
/**
 * Created by PhpStorm.
 * User: serg
 * Date: 20.01.16
 * Time: 15:35
 */

$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_NAME;
try {
    $pdo = new PDO($dsn,DB_USER,DB_PASS);
} catch ( Exception $exp) {
    $errorText = 'Can\'t connect to DB - '.$exp->getMessage();
    echo $errorText.'<br><script> console.log("'.$errorText.'")</script>';
}
