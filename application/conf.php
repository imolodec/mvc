<?php
$vars = [];
/*
* The database host URL
*/
$vars['DB_HOST'] = 'localhost';
/*
* The database username
*/
$vars['DB_USER'] = 'root';
/*
* The database password
*/
$vars['DB_PASS'] = 'root';
/*
* The name of the database to work with
*/
$vars['DB_NAME'] = 'mvc';
