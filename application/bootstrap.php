<?php
/**
 * Created by PhpStorm.
 * User: serg
 * Date: 25.12.15
 * Time: 11:52
 */

require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';
include_once 'conf.php';
require_once 'helper/helper.php';

foreach ($vars as $name => $val)
{
  define($name, $val);
}

Route::$counter++;
Route::start(); // запускаем маршрутизатор