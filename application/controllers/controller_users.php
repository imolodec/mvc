<?php
/**
 * Created by PhpStorm.
 * User: serg
 * Date: 20.01.16
 * Time: 16:50
 */

class Controller_Users extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function action_login()
    {
        $this->view->generate('login_view.php', 'template_view.php',['pageTitle'=>'Авторизация']);
    }


    /*
     * function return code
     * 200 - good
    */

    function action_auth()
    {
        if (array_key_exists('login',$_REQUEST) && array_key_exists('password', $_REQUEST)){

            $user = new Users();
            $result = $user->auth($_REQUEST['login'], $_REQUEST['password']);

            if ($result == true) {
                echo '200';
            } else {
                echo 'auth error <br>';
                $this->view->generate('login_view.php', 'template_view.php', ['pageTitle' => 'Авторизация']);
            }

            //return $result;
        } else {
            echo 'Ошибка передачи параметров <a href="/users/login">Авторизация</a>';
        }
    }

    function action_profile()
    {
        if (!Users::check_auth()) {
            $this->action_login();
            return;
        }
        if (array_key_exists('access_key',$_COOKIE)) {
            $user = Users::getUserByKey($_COOKIE['access_key']);
            if ($user instanceof Users) {
                $this->view->generate('profile_view.php', 'template_view.php', ['pageTitle' => $user->login,
                    'user' => $user]);
            } else {
                $this->action_login();
            }
        } elseif (array_key_exists('facebook_access_token', $_SESSION)) {
            $user = new Users();
            $fbInfo = $user->getUserFacebookInfo();

            $user->login = $fbInfo->getName();
            $user->email = $fbInfo->getEmail();
            $user->id = $fbInfo->getId();

            $this->view->generate('profile_view.php', 'template_view.php', ['pageTitle' => 'User profile',
                'user' => $user]);
        } elseif (isset($_COOKIE['google_access_token'])) {

            if (isset($_COOKIE['user'])) {
                $user = json_decode($_COOKIE['user']);
            } else {
                $user = new Users();
            }

            $this->view->generate('profile_view.php', 'template_view.php', ['pageTitle' => 'User profile',
                'user' => $user]);
        }
        else {
            $this->action_login();
        }

    }


    function action_registration()
    {
        if (array_key_exists('login', $_REQUEST) && array_key_exists('password', $_REQUEST)) {
            Users::registerNewUser($_REQUEST['login'], $_REQUEST['password']);
        }
    }

    function action_logoff()
    {
        Users::logOff();

        header("Location: /main/index");
    }


    function action_postFb()
    {
        $fb = Helper::getFB();

        $data = [
            'message' => 'My awesome photo upload example.',
            'source' => $fb->fileToUpload('public/logo.jpg'),
        ];

        $accessToken = Helper::getFBToken($fb);

        if (!isset($accessToken)) {
            echo 'FB access token error!';
            return;
        }

        try {
            $response = $fb->post('/me/photos', $data, $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

        echo 'Photo ID: ' . $graphNode['id'] . ' post FB';

        header("Location: /main/index");
    }

}