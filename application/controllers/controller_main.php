<?php
/**
 * Created by PhpStorm.
 * User: iMolodec
 * Date: 12/25/15
 * Time: 11:14 PM
 */
class Controller_Main extends Controller
{
    function action_index()
    {
        $this->view->generate('main_view.php', 'template_view.php');
    }

    function action_getSessionParam()
    {
        if (session_status() != PHP_SESSION_ACTIVE){
            session_start();
        };

        if (array_key_exists('name', $_REQUEST)){
            $name = $_REQUEST['name'];
        } else {
            return;
        }

        if (array_key_exists($name,$_SESSION)){
            echo $_SESSION[$name];
        }

    }

    function action_web()
    {
        require_once 'application/helper/webservice_client.php';
        $web = new WebClient;
        $web->setUrl('https://gisapiapp.srv.mhp.com.ua/gis/ws/gis_ws.1cws?wsdl');
        $webdata = $web->web_request();

        $this->view->generate('web_view.php', 'template_view.php',['data'=>$webdata]);

    }

    function action_test()
    {
        $arr = ['aa'=>['aaa','bbb','ccc'],'bb'=>['ddd','eee','fff'],'cc'=>['ggg','hhh','iii']];
        $newArr0 = [];
        $newArr1 = [];
        foreach ($arr  as $key=>$value) {
            foreach($value as $key_in=>$value_in){
                $newArr0[$key_in][] = $value_in;
            }
        }

        $keys = array_keys($arr);

        for($i=0;$i<count($arr);$i++){
            $newArr1[$keys[$i]] = array_column($arr,$i);
        }

        $instA = new A();
        $instB = new B();
        $instA::$val = 10;
        $instB::$val = 20;
        $instA->val1 = 10;
        $instB->val1 = 20;

        echo 'A - ' . $instA::$val;
        echo '<br>A  self - ' . $instA::get_self();
        echo '<br>A  static - ' . $instA::get_static();
        echo '<br>B - ' . $instB::$val;
        echo '<br>B  self - ' . $instB::get_self();
        echo '<br>B  static - ' . $instB::get_static();

    }

}

class A {

    public static $val;
    public $val1;

    public static function get_self() {
        return self::$val;
    }

    public static function get_static() {
        return static::$val;
    }
}

class B extends A {
    //public static $val;
    public static function get_self()
    {
        echo '<br>parent A self-  ' . parent::get_self();
        return self::$val;
    }
}