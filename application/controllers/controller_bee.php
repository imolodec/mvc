<?php
/**
 * Created by PhpStorm.
 * User: iMolodec
 * Date: 1/12/16
 * Time: 8:55 PM
 */

require "application/helper/game_bee.php";

class Controller_Bee extends Controller
{
    public static $auth_actions=['action_index'];

    function action_index()
    {
        include_once "application/models/model_users.php";

        $game_bee = new Game_Bee();

        //save to session
        $_SESSION['game_bee'] = $game_bee;

        $this->view->generate('bee_view.php', 'template_view.php',['bees'=>$game_bee]);
    }

    function action_damage()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_SESSION['game_bee'])) {
            $game_bee = $_SESSION['game_bee'];
        } else {
            $game_bee = new Game_Bee();
            $_SESSION['game_bee'] = $game_bee;
        }

        try {
            $game_bee->hitBee();
        } catch (Exception $ex) {
            $text = 'error - '. $ex->getMessage();
        }
    }
}