<?php

/**
 * Created by PhpStorm.
 * User: serg
 * Date: 20.01.16
 * Time: 14:58
 */
class Users extends Model
{
    public $login;
    public $email;
    public $id;
    private static $saltLength = 5;

    public function __construct($login = '', $email = '', $id = '')
    {
        $this->login = $login;
        $this->email = $email;
        $this->id = $id;
    }

    static public function getUserByKey($access_key = '')
    {
        require "application/core/db_connect.php";
        $db_data = $pdo->query('select * from users where access_key = "' . $access_key . '"');
        $user_row = array_shift($db_data->fetchall());

        if ($user_row != null) {
            $user = new static($user_row['login'], $user_row['email'], $user_row['id']);
            Helper::setCookieFromArr(['user' => json_encode($user), 'access_key' => $access_key]);
        } else {
            return null;
        }

        $pdo = null;
        $db_data = null;
        return $user;
    }

    public function auth($login, $password)
    {
        require "application/core/db_connect.php";

        $data = $pdo->query('select * from users where login = "' . $login . '"');

        if ($data != false) {

            $user_row = array_shift($data->fetchall());
            $pdo = null;
            $data = null;
            $hashPass = static::getSaltedHash($password, $user_row['password']);

            if ($user_row != null && $user_row['password'] == $hashPass) {
                $this->login = $user_row['login'];
                $this->email = $user_row['email'];
                $this->id = $user_row['id'];

                $_SESSION['access_key'] = $user_row['access_key'];

                Helper::setCookieFromArr(['user' => json_encode($this), 'access_key' => $user_row['access_key']]);

                return true;
            }
        } else {
            echo '<script> console.log("get data from DB error")</script>';
        }

        return false;
    }

    public static function check_auth($returnUser = false)
    {
        if (static::check_authFB() || static::check_authGoogle()) {
            return true;
        }

        if (array_key_exists('access_key', $_COOKIE)
            && array_key_exists('access_key', $_SESSION)
            && $_COOKIE['access_key'] === $_SESSION['access_key']
        ) {
            $user = static::getUserByKey($_COOKIE['access_key']);

            if ($user == null) {
                Helper::logOff();
                return false;
            }

            if ($returnUser == true) {
                return $user;
            }

            return true;
        }

        return false;
    }

    public static function check_authFB()
    {

        $fb = Helper::getFB();

        $jsHelper = $fb->getJavaScriptHelper();
        $accessToken = null;

        try {
            $accessToken = $jsHelper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
        }

        if (isset($accessToken)) {
            // Logged in!
            $_SESSION['facebook_access_token'] = (string)$accessToken;

            // Now you can redirect to another page and use the
            // access token from $_SESSION['facebook_access_token']
            return true;
        }

        return false;
    }

    public static function check_authGoogle()
    {
        if (!isset($_COOKIE['google_access_token'])) {
            return false;
        }
        $access_token = $_COOKIE['google_access_token'];
        $client = new Google_Client();
        $client->setAuthConfigFile('application/keys/client_google.json');
//        $client->setAccessToken($access_token);
        try {
            $ticket = $client->verifyIdToken($access_token);
        } catch(Exception $ex) {
            $erroMessage = $ex->getMessage();
            //echo $erroMessage;
            return false;
        }

        if ($ticket) {
            $data = $ticket->getAttributes();
            return $data['payload']['sub']; // user ID
        }


    }

    public function authGoogle() {

        $client = new Google_Client();
        $client->setAuthConfigFile('application/keys/client_google.json');
        $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/main/index');


        if (!isset($_GET['code'])) {
            $access_token = $_COOKIE['google_access_token'];
            $client->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY);
            $auth_url = $client->createAuthUrl();
            header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
        } else {
            $code = $_GET['code'];
            $client->authenticate($code);
            $access_token = $client->getAccessToken();
            $client->setAccessToken($access_token);

            $drive_service = new Google_Service_Plus($client);
            $drive_service->people->get();
        }

        return true;
    }

    public function getUserFacebookInfo()
    {
        if (!array_key_exists('facebook_access_token', $_SESSION)) {
            return null;
        }

        $fb = Helper::getFB();
        $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);

        try {
            $response = $fb->get('/me?fields=id,name,birthday,email,hometown');
            $userNode = $response->getGraphUser();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        return $userNode;
    }

    /*
     * function return code
     * 200 - good
     * 400 - user already exist
     * 500 - error insert to DB
     *
     */
    public static function registerNewUser($login, $password)
    {
        require "application/core/db_connect.php";
        $data = $pdo->query('select * from users where login = "' . $login . '"');

        if ($data->rowcount() != 0) {
            echo 400;
            return;
        }

        $access_key = uniqid(substr(md5(time()), 0, 5), true);

        $hash = static::getSaltedHash($password);

        $data = $pdo->query('insert into users (login,password,access_key) VALUES(\'' . $login . '\',\'' . $hash . '\', \'' . $access_key . '\')');

        if ($data == false) {
            echo 500;
            return;
        }

        echo 200;

    }

    public static function getSaltedHash($str, $salt = NULL)
    {
        if ($salt == NULL) {
            $salt = substr(md5(time()), 0, static::$saltLength);
        } else {
            $salt = substr($salt, 0, static::$saltLength);
        }

        $hash = $salt . sha1($str);

        return $hash;
    }

    public static function logOff()
    {
        unset($_SESSION['access_key']);
        unset($_SESSION['facebook_access_token']);
        setcookie("user", '', 0, '/');
        setcookie("access_key", '', 0, '/');
        setcookie("google_access_token", '', 0, '/');

        //  $dateExpiresSA = date("D, d M Y H:i:s", time() - 60 * 10);
        //  header('Set-cookie: user= ""; expires=' . $dateExpiresSA . ' GMT; path=/;');

    }

}
