<?php

/**
 * Created by PhpStorm.
 * User: iMolodec
 * Date: 1/12/16
 * Time: 9:06 PM
 */

abstract class Bee extends Model
{

    private $lifespan;
    private $hit;

    public static function initial($bee, $lifespan, $hit)
    {
        if ($bee == 'Queen_Bee') {
            $inst = Queen_Bee::getInstance($lifespan, $hit);
        } else {
            $inst = new $bee($lifespan, $hit);
        }
        return $inst;
    }

    protected function __construct($lifespan, $hit)
    {
        $this->lifespan = $lifespan;
        $this->hit = $hit;
    }

    public function hitMe()
    {
        $this->lifespan -= $this->hit;
    }

    public function checkLife()
    {
        return $this->lifespan >= 0;
    }

    public function getLife()
    {
        return $this->lifespan;
    }

}

class Queen_Bee extends Bee
{

    private static $instance;

    protected function __construct($lifespan, $hit)
    {     // Защищаем от создания через new Singleton
        parent::__construct($lifespan, $hit);
    }

    protected function __clone()
    { /* ... @return Singleton */
    }  // Защищаем от создания через клонирование

    public function __wakeup()
    { /* ... @return Singleton */
    }  // Защищаем от создания через unserialize

    static public function getInstance($lifespan, $hit)
    {
        if (empty(static::$instance)) {
            static::$instance = new static($lifespan, $hit);
        }

        return static::$instance;
    }

}

class Worker_Bee extends Bee
{

}

class Drone_Bee extends Bee
{

}
