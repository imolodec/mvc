<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="549551522124-lepnprsm1jbp7l0ksj3g16kdesvjgfod.apps.googleusercontent.com">
    <title><?php echo isset($pageTitle)? $pageTitle : 'Главная'?></title>
    <script type="text/javascript" src="/public/js/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/public/css/app.css">
    <script type="text/javascript" src="/public/js/auth.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script type="text/javascript" src="/public/js/facebook.js"></script>
    <script type="text/javascript" src="/public/js/google.js"></script>
    <script type="text/javascript" src="/public/js/app.js"></script>
</head>
<body>

    <?php
        include_once 'application/views/global/login_modal.php';
        include_once 'application/views/global/main_navbars.php';
    ?>

    <div class="container">
        <!-- Main component for a primary marketing message or call to action -->
        <div class="jumbotron">

