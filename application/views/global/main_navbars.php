    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar">1</span>
                    <span class="icon-bar">2</span>
                    <span class="icon-bar">3</span>
                </button>
                <a class="navbar-brand" href="#">R&K</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/main/index">Home</a></li>
                    <li><a href="/portfolio/index">About</a></li>
                    <li><a href="/bee/index">Пчёлки</a></li>
                    <li><a href="/main/web">WEB-Service</a></li>
                    <li><a href="/portfolio/desc">File</a></li>
                    <li><a href="/users/postFB">Post FB</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#">Default</a></li>
                    <li><a href="#">Static top</a></li>
                    <li class="hidden" data-toggle="modal" data-target="#SignInModal" id="sign-in-li"><a href="#">Sign in <!--<span class="sr-only">(current)</span>--></a></li>
                    <li class="hidden" id="user-profile-li" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span id="user-name-link">User</span> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a id="user-name-link" href="/users/profile">Profile</a></li>
                            <li><a href="#" class="my-logoff-button" onclick="signOutGoogle();">Logoff</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header">Actions</li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>