    <!-- Modal -->
    <div class="modal fade" id="SignInModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close-modal-button"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Authentification</h4>
                </div>

                <div class="modal-body">

                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#auth" aria-controls="auth" role="tab" data-toggle="tab">Authentification</a></li>
                            <li role="presentation"><a href="#registration" aria-controls="registration" role="tab" data-toggle="tab">Regitration</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="auth">
                                <div id="login-block">
                                    <form method="POST" action="/users/auth">
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Login:</label>
                                            <input type="text" class="form-control" id="recipient-name" placeholder="Login" name="login" value="iMolodec">
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="control-label">Password:</label>
                                            <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password" value="123">
                                        </div>

                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Remember me
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary form-control" name="login-button" id="login-button" data-dismiss="modal">Sign in</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="registration">

                                <div id="reg-block">
                                    <form method="POST" action="/users/registration">
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Login:</label>
                                            <input type="text" class="form-control" id="new-recipient-name" placeholder="Login" name="new-login" value="iMolodecTest1">
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="control-label">Password:</label>
                                            <input type="password" class="form-control" id="new-inputPassword" placeholder="Password" name="new-password" value="123">
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary form-control" name="reg-button" id="reg-button" data-dismiss="modal">Register</button>
                                        </div>
                                    </form>
                                </div>

                            </div>

                        </div>

                    </div>


                    <!--
                      Below we include the Login Button social plugin. This button uses
                      the JavaScript SDK to present a graphical Login button that triggers
                      the FB.login() function when clicked.
                    -->
                    <div id="login-button-block">
                        <fb:login-button data-size="xlarge"  id="fb-login-button" scope="public_profile,email,user_likes,user_birthday,user_hometown,publish_actions" onlogin="checkLoginState();">
                        </fb:login-button>

                        <br>

                        <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
                    </div>

                    <div id="info-block">

                    </div>

                </div>
            </div>
        </div>
    </div>