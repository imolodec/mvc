<?php

/**
 * Created by PhpStorm.
 * User: iMolodec
 * Date: 1/12/16
 * Time: 9:53 PM
 */


class Game_Bee
{

    public $bees;

    public function __construct()
    {

        $this->start();

    }

    private function start()
    {
        $this->bees = [];
        $this->bees[] = Bee::initial('Queen_Bee',100,25);//8);

        for ($i=0;$i<3;$i++){
            $this->bees[] = Bee::initial('Worker_Bee',75,25);//10);
        }

        for ($i=0;$i<5;$i++){
            $this->bees[] = Bee::initial('Drone_Bee',50,25);//12);
        }
    }

    public function hitBee()
    {
        $key = array_rand($this->bees);

        $beeForHit = $this->bees[$key];
        $beeForHit->hitMe();

        $className = get_class($beeForHit);

        if (!$beeForHit->checkLife()) {
            if ($beeForHit instanceof Queen_Bee) {
                $this->start();
                $text = 'Qween die!!! <br> New Level ...';
            } else {

                unset($this->bees[$key]);
                $text = 'it was hurt ' . $key . ' - ' . $className;
            }

        } else {
            $text = 'oh... ' . $key . ' - ' . $className . '(' . $beeForHit->getLife() . ')';
        }

        echo $text;
    }

}

$p = 'q';
$pp=1;
