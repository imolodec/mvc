<?php
/**
 * Created by PhpStorm.
 * User: iMolodec
 * Date: 1/12/16
 * Time: 11:18 PM
 */
    include_once "../core/model.php";
    include_once "game_bee.php";
    include_once "../models/model_bee.php";

    if (session_status() != PHP_SESSION_ACTIVE){
        session_start();
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_SESSION['game_bee'])) {
        $game_bee = $_SESSION['game_bee'];
    } else {
        $game_bee = new Game_Bee();
        $_SESSION['game_bee'] = $game_bee;
    }

    try {
        $game_bee->hitBee();
    } catch (Exception $ex) {

        $text = 'error - '. $ex->getMessage();
    }