<?php

/**
 * Created by PhpStorm.
 * User: iMolodec
 * Date: 1/26/16
 * Time: 11:36 PM
 */
class Helper
{

    public static function setCookieFromArr($data)
    {

        $session_active = session_status() != PHP_SESSION_ACTIVE;

        $dateExpiresSA = date("D, d M Y H:i:s", time() + 60 * 10);

        $dateExpires = time() + 60 * 1;

        foreach ($data as $key=>$value) {
            if ($session_active==false) {

                setcookie($key, $value, $dateExpires, '/');
            } else {
                header('Set-cookie: ' . $key . '=' . $value . '; expires=' . $dateExpiresSA . ' GMT; path=/;');
            }
        }
    }

    public static function getFB()
    {
        $fb = new Facebook\Facebook([
            'app_id' => '523664607814748',
            'app_secret' => '117cd5e6554fc0d42331aafde3abe6f8',
            'default_graph_version' => 'v2.5',
        ]);
        return $fb;
    }

    public static function getFBToken (\Facebook\Facebook $fb)
    {
        $jsHelper = $fb->getJavaScriptHelper();
        $accessToken = null;
        try {
            $accessToken = $jsHelper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
        }
        return $accessToken;
    }
}