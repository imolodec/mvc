<?php
/* Web service client */

class WebClient
{
    protected $url = "http://10.11.73.64/RemnantsOfTheGoodsItEnterprise/ws/RemnantsOfTheGoodsItEnterprise?wsdl";

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function web_request(){

        $client = new SoapClient($this->url,['connection_timeout'=> 300]);

        $param ="<?xml version='1.0' encoding='UTF-8'?><КорневойЭлементПараметры xmlns='https://new.portal.mhp.com.ua/RemnantsOfTheGoodsItEnterprise'><ДатаОстатков>2015-12-23T11:45:08</ДатаОстатков><КодыНоменклатуры><КодНоменклатуры>КодИТ1</КодНоменклатуры><КодНоменклатуры>КодИТ2</КодНоменклатуры><КодНоменклатуры>КодИТ3</КодНоменклатуры><КодНоменклатуры>КодИТ4</КодНоменклатуры></КодыНоменклатуры><КодыСкладов><КодСклада>ЦБ10112093</КодСклада><КодСклада>ЦБ-0001521</КодСклада><КодСклада>бэбэбэ</КодСклада></КодыСкладов></КорневойЭлементПараметры>";

        $resp = ['return'=>'don\'t work'];
        try {

            //$resp = $client->Get(['СтруктураПараметров'=>$param]);
            $resp = $client->Test();

        } catch(Exception $e){
            echo $resp;
            echo '<br>';
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
            echo '<br>';
            $resp = '';
            return $resp;
        }

        return $resp->return;

    }

}

